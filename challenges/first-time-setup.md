# First Steps

- create a 'challenges' folder on your computer
- initialize the folder as a git repo
- publish your git repo to a public repository

# Javascript

- [make sure node is installed](https://nodejs.org/en/download/)

- npm init your project
- npm install eslint
- npm install prettier

**if Vscode**

- install eslint package
- install prettier package

**if Atom**

- install fast-eslint package
- install beautify or prettier

# Python

$ virtualenv -p python3 env
$ . env/bin/activate
(env) $ pip install -r requirements.txt # (optional)
(env) $ python main.py
