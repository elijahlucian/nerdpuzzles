# Getting Started

These challenges are web-centric, meaning you need some knowledge of html, css, and javascript to finish them.

Before you jump to using a framework like React, Angular, Vue or Svelte to complete these challenges, you should probably solve them with vanilla html/css/javascript first to make sure you understand the underlying foundations upon which the above frameworks are built.

## Challenge 1

- Make a text `input` box that takes user input
- output the user input in real time between a `h1` tag

## Challenge 2

- Make a text `input` box that takes user input
- output the user input in real time in reverse between an `h1` tag

## Challenge 3

- Make a text `input` box that takes user input
- Make a `button` beside the text box
- when the user `clicks` the button, the text is outputted on the page as an `h1` tag
- if the user presses the "enter" button, it should have the same `functionality` as the button
- if the text box is empty, no `h1` tag is created

## Challenge 4

- put an `event` listener on the window that listens to the user's keystrokes
- when the user types a key, create a `button` with the letter of the key they pressed in uppercase, make the button a random color.

## Challenge 5

- put two number `input` boxes on a page
- put 4 `buttons` with different mathematical operators on them
- when the user presses a button, the desired result is outputted between an `h1` tag with the id of 'result'.

## Challenge 6

- make a basic calculator with the digits 0-9, 4 mathematical operators, a "clear" button and an "equals" button.
- add an output box that shows the result of each keypress and then the result.
- make the output box have a unique google font!

## Challenge 7

- publish each of these challenges to heroku.

## Challenge 8

- make the user enter an email, password, and password confirmation and a submit button
- when the user clicks the submit button it should make sure the passwords match, and the email is valid.
- if all fields are valid, show "success!" somewhere on the page (not an alert box), otherwise show an appropriate error message
