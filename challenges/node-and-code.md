# Getting Started

As a pre-requisite, you should know how to make a [Git repo](./git.md), and push your changes to a repository.

this challenge requires using VSCode.

## Challenge 1

- Make a new node project
- Push your node project to git(lab/hub)

## Challenge 2

- create a javascript file in the `src` directory
- console.log hello world.
- run the script with the `node` command in your terminal

## Challenge 3

- create a start script in package.json that has the command from the last challenge
- run the start script from the command line

## Challange 4

**Installing & USing NPM Packages**

- create a new script and follow this tutorial: https://hackernoon.com/9-npm-packages-to-explore-in-your-next-node-js-application-2018-ec4aac0ca723

## Challenge 5

- install the `eslint` package
- install the `prettier` package
- in vscode, go to extensions and install the `eslint` and `prettier` extensions
- go to one of your files and save
- if a dialog pops up, click "allow everywhere"

## Challenge 6

- format your code with prettier.

## Challenge 7

- enable "format on save" in vscode.

## Challenge 8

- use the font "Fira Code" in vscode
- enable font ligatures

## Challenge 9

- create a branch with vscode's source control tools (the GUI)
- publish the branch with the GUI
- commit and push your changes with the GUI
