/* eslint no-eval: 0 */
import { useEffect, useRef, useState } from 'react';
import codemirror from 'codemirror'
import 'codemirror/mode/javascript/javascript'
import 'codemirror/lib/codemirror.css'

const App = () => {
  const ref = useRef<HTMLDivElement | null>(null)
  const [status, setStatus] = useState('')
  const [pop, setPop] = useState(false)


  useEffect(() => {
    if (!pop) return
    setTimeout(() => {
      setPop(false)
    }, 1150)
  }, [pop])

  useEffect(() => {
    if (!ref) return

    const el = ref.current as HTMLElement

    const codeMirror = codemirror(el, {
      lineNumbers: true,
      mode: 'javascript',
    })

    codeMirror.on('change', function (e, v) {
      // console.log(e, v)
    })


    codeMirror.focus()

    const handleShortcut = (key: string) => {
      if (key === 's') {
        setStatus("Saved!")
        setPop(true)
      }
      if (key === 'Enter') {
        try {
          eval(codeMirror.getValue())
        } catch (err) {
          setStatus("Code is invalid!")
          setPop(true)
          console.error(err)
        }
      }
    }

    const saveHandler = (e: KeyboardEvent) => {
      if ((e.ctrlKey || e.metaKey)) {
        if (!['s', 'Enter'].includes(e.key)) return
        handleShortcut(e.key)
        e.preventDefault()
        return false
      }
    }

    document.addEventListener('keydown', saveHandler)

    return () => {
      const child = el.firstChild
      if (child) el.removeChild(child)
      document.removeEventListener('keydown', saveHandler)
    }

  }, [ref])

  return (
    <div className="app">
      <div className="header">
        <div className="top-mid">
          <h1>🧩 Nerdpuzzles</h1>
          <p>write some code!</p>
        </div>
      </div>
      <div className="code-box" ref={ref}></div>
      <div className="footer">
        {/* <button>Run Code!</button> */}
        <p className={`status ${pop ? 'pop' : ''}`}>{status}</p>
      </div>
    </div>
  );
};

export default App;
